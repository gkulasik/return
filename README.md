# README #

Calculator built for the Return game. Return is a game developed by myself and a couple of classmates basically a mix of Risk and Settlers of Catan. 
The game requires a good number of mathematical calculations and in order to speed up the play time I've developed this JS based calculator.

Visit at [returnapp.herokuapp.com](https://returnapp.herokuapp.com)